import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { Profile } from './profile';

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {
  private url = 'https://profiles-list.firebaseio.com/Data.json';
  // Using subject instead of traditional Promise for the future backend pagination and sorting
  private profilesSubject = new BehaviorSubject([]);

  constructor(private http: HttpClient) {
  }

  /**
   * Load all the profiles and pass the data to to Subject
   */
  private loadProfiles(): void {
    this.http.get(this.url).subscribe((data: any) => {
      this.profilesSubject.next(data);
    }, (err: any) => {
      console.error('API Error:', err.message);
    });
  }

  /**
   * Returns Observable Subject
   */
  public getProfiles(): Observable<object> {
    if (!this.profilesSubject.getValue().length) {
      this.loadProfiles();
    }

    return this.profilesSubject.asObservable();
  }

  /**
   * Backend Error handler
   * @param error
   */
  public errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || 'Server Error');
  }

  /**
   * Gets the profile from BehaviorSubject by ID
   * @param id
   */
  public getProfile(id: number): Profile {
    return this.profilesSubject.getValue().filter(profile => profile.localid === id)[0];
  }
}
