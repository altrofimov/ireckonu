import { Component, ViewChild, OnDestroy, OnInit } from '@angular/core';
import { ProfilesService } from '../profiles.service';
import { Profile } from '../profile';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnDestroy, OnInit {
  private displayedColumns = [
    'avatar',
    'localid',
    'email',
    'first_name',
    'last_name',
    'phone',
    'address',
    'modified',
    'view'
  ];
  private profilesSubscription: Subscription;
  public dataSource: MatTableDataSource<Profile>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private profilesService: ProfilesService) {
  }

  ngOnInit() {
    this.profilesSubscription = this.profilesService.getProfiles().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, (err: any) => {
      console.error('API Error:', err.message);
    });
  }

  /**
   * Filter the data in Profiles table
   * @param searchValue
   * @param $event
   */
  profilesSearch(searchValue: string, $event?: Event) {
    searchValue = searchValue.trim();
    searchValue = searchValue.toLowerCase();
    this.dataSource.filter = searchValue;
    $event.preventDefault();
  }

  ngOnDestroy() {
    this.profilesSubscription.unsubscribe();
  }
}
