import { Component, OnInit } from '@angular/core';
import { ProfilesService } from '../profiles.service';
import { Profile } from '../profile';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public profile: Profile;

  constructor(private profilesService: ProfilesService, private router: Router, private route: ActivatedRoute ) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(() => {
      const profileId = this.route.snapshot.params.id;
      this.profile = this.profilesService.getProfile(+profileId);

      // Workaround for the Test task. Because there is no API to get a single profile, if user opens page of single profile without
      // loading all the profiles user is returned to the page with list of profiles
      if (!this.profile) {
        this.router.navigate(['/profiles']);
      }
    });
  }
}
